#pragma once

#include "Arduino.h"

#define LENGTH(x) (sizeof(x)/sizeof(*(x)))
#define BUFF_SIZE 32

struct  temp_buffer
{
    int subbuffer[BUFF_SIZE];
};


class Temperature
{
public:
    Temperature(uint8_t pins[])
    {
        this->pins_count = LENGTH(pins);
        this->pins = new uint8_t[pins_count];
        this->buffer = new temp_buffer[pins_count];
        this->pins = pins;

        /*for(unsigned i = 0; i < pins_count; i++)
            pinMode(this->pins[i], OUTPUT);*/
    }

    Temperature() 
    {
        uint8_t analog_pins[8] = {A0, A1, A2, A3, A4, A5, A6, A7};
        this->pins_count = LENGTH(analog_pins);
        this->pins = new uint8_t[pins_count];
        this->pins = analog_pins;
    }

    float getInCelsius(unsigned id);
    float getInFahrenheit(unsigned id);

    void readTemp();

    inline size_t getTempPins() { return LENGTH(pins); }

private:
    uint8_t * pins;
    temp_buffer * buffer;
    size_t pins_count;

};