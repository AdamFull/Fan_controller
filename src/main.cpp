#include <Arduino.h>
#include "temperature.h"


uint8_t temperature_pins[1] = {A0};
Temperature tmp(temperature_pins);

void setup() 
{
  Serial.begin(9600);
}

void loop() 
{
  tmp.readTemp();
  Serial.println(tmp.getInCelsius(0));
}