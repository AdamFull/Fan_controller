#include "temperature.h"

void Temperature::readTemp()
{
    for(unsigned i = 0; i < pins_count; i++)
    {
        for(unsigned j = 0; j < BUFF_SIZE; j++)
            buffer[i].subbuffer[j] = analogRead(pins[i]);
    }
}

float Temperature::getInFahrenheit(unsigned id)
{
    int avg = 0; float temperature = 0;
    for(unsigned i = 0; i < BUFF_SIZE; i++)
        avg += buffer[id].subbuffer[i];
    avg = avg/BUFF_SIZE;
    temperature = log((10240000/avg)-10000);
    temperature = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * pow(temperature, 2))) * temperature);
    return temperature;
}

float Temperature::getInCelsius(unsigned id)
{
    return getInFahrenheit(id) - 273.15;
}